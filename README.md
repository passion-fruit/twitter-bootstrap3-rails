# Twitter Bootstrap 3 for Rails
=========

This is a very immature rails engine that contains the Javascript and CSS needed for Bootstrap 3.

This is a work in progress, so if you use it, use it at your peril :)


##Installation
=========

There are two ways you can install this gem.

### Cloning the repo
You can clone the repo and build the gem, like so:

``` sh
git clone git@github.com:krlgrgn/twitter-bootstrap3-rails.git
cd twitter-bootstrap3-rails
gem build twitter_bootstrap3_rails.gemspec
```

Then you just need to use ``` gem install ``` and point to the path that contains your compiled gem.


### Adding it to your Gemfile (and the easier approach)

Add it you rails app's Gemfile like this:

``` sh
gem 'twitter_bootstrap3_rails', git: 'git@github.com:krlgrgn/twitter-bootstrap3-rails.git'
```

Run ```bundle```

Add this line to your application.css
``` sh
 *= require twitter_bootstrap3_rails
```


Add this line to your applcation.js
``` sh
 //= require twitter_bootstrap3_rails
```

__N.B.__ I added these before ` require_tree ` directive just so that any custom code I had would not be overwritten.

Finally start your rails app!